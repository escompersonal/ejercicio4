import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class ClienteBD{
	 private Socket socket;

	 


	/*public Object sendRequest(int request, Categoria c)throws IOException, ClassNotFoundException{    	
    	input = new ObjectInputStream(socket.getInputStream());
    	output = new ObjectOutputStream(socket.getOutputStream());
    	output.writeInt(request);
    	output.writeObject(c);
    	input.close();
    	output.close();
    	return input.readObject();    	
    }*/

	public Object sendRequest(int request)throws IOException, ClassNotFoundException{ 
		socket = new Socket("localhost",ServidorBD.PORT);
		
		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream()); 
    	ObjectInputStream input = new ObjectInputStream(socket.getInputStream());    	

    	  
    	output.writeInt(request);
    	output.flush();
    	Object list = (List) input.readObject();
    	input.close();
    	output.close();    	
    	return list;
    }


    public Object sendRequest(int request, Categoria ca)throws IOException, ClassNotFoundException{ 
    	socket = new Socket("localhost",ServidorBD.PORT);

		
		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream()); 
    	ObjectInputStream input = new ObjectInputStream(socket.getInputStream());     	  
    	output.writeInt(request);
    	output.writeObject(ca);
    	output.flush();

    	Object o = input.readObject();
    	input.close();
    	output.close();    	
    	return o;
    }

    public void close()throws IOException{
    	if(socket != null)
    		socket.close();
    }

   



	
}