import java.io.Serializable;

public class Categoria implements Serializable{
    private int idCategoria;
    private String nombCategoria;
    
    public Categoria(int idCategoria, String nombCategoria) {
        this.idCategoria = idCategoria;
        this.nombCategoria = nombCategoria;
    }

    public Categoria(){ 
        super();       
    }

    public int getId() {
        return idCategoria;
    }

    public String getNombre() {
        return nombCategoria;
    }

    public void setId(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public void setNombre(String nombCategoria) {
        this.nombCategoria = nombCategoria;
    }

    @Override
    public String toString() {
        return getId()+"    :   "+getNombre();
    }

}