import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

class ManagerConexion implements InterfaceCRUD{

	private Connection cnn;
	private CategoriaDAO dao;
    public ManagerConexion() {
        String user = "root";
        String pwd = "root";
        String url = "jdbc:mysql://localhost:3306/Distribuidos";
        String mySqlDriver = "com.mysql.jdbc.Driver";
        dao = new CategoriaDAO();
        try {
            Class.forName(mySqlDriver);
            cnn = DriverManager.getConnection(url, user, pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public boolean alta(Categoria c)throws SQLException{
		return dao.create(c,cnn);
	}
    public boolean baja(Categoria c)throws SQLException{
    	return dao.delete(c,cnn);
    }
    public boolean editar(Categoria c)throws SQLException{
    	return dao.update(c,cnn);
    }
    public Categoria buscar(Categoria c)throws SQLException{
    	return dao.load(c,cnn);
    }
    public List<Categoria> listar()throws SQLException{
    	return dao.loadAll(cnn);
    }
	
}