import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameCategoria extends JFrame{

	PanelCategoria pc = new PanelCategoria();

	public FrameCategoria(){		
		super("Categoria");
		this.add(pc);
		setSize(500,450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);	
		setVisible(true);
	}
}