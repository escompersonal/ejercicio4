
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.List;

public class PanelCategoria extends JPanel {
	private JLabel labelIdCat;
	private JLabel labelNombCat;
	private JTextField fieldId;
	private JTextField fieldNom;
	private JTextArea areaResp;
	private JButton btnAgrg;
	private JButton btnElim;
	private JButton btnBusc;
	private JButton btnList;
	private JButton btnModif;
	
	private List <Categoria> list;

	public PanelCategoria(){
		labelIdCat = new JLabel("Id");
		labelNombCat = new JLabel("Nombre");
		fieldId = new JTextField();
		fieldNom = new JTextField();
		areaResp = new JTextArea();
		btnModif = new JButton("Modificar");
		btnList = new JButton("Listar");
		btnBusc = new JButton("Buscar");
		btnElim = new JButton("Eliminar");
		btnAgrg = new JButton("Agregar");

		init();		
	}

	public void init(){
		setLayout(null);
		 

		labelIdCat.setBounds(80,50,60,20);
		labelNombCat.setBounds(40,90,55,20);
		fieldId.setBounds(110,50,140,20);
		fieldNom.setBounds(110,90,140,20);

		
		btnAgrg.setBounds(330,10,120,20);
		btnElim.setBounds(330,40,120,20);
		btnBusc.setBounds(330,70,120,20);
		btnModif.setBounds(330,100,120,20);
		btnList.setBounds(330,130,120,20);
		
		areaResp.setBounds(30,180,440,200);
		areaResp.setEditable(false);

		presentarDatos();

		add(btnAgrg);
		add(btnModif);
		add(btnList);
		add(btnBusc);
		add(btnElim);

		add(labelNombCat);
		add(labelIdCat);
		add(fieldNom);
		add(fieldId);

		add(areaResp);

		btnAgrg.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				presentarDatos(ServidorBD.CREATE);              
            }
		});

		btnElim.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				presentarDatos(ServidorBD.DELETE);                
            }
		});

		btnModif.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				presentarDatos(ServidorBD.UPDATE);                
            }
		});

		btnBusc.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				presentarDatos(ServidorBD.LIST);                
            }
		});

		btnList.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				presentarDatos();
            }
		});		
	}

	public void presentarDatos(int request){
		Categoria c = new Categoria();
        int id_cat = Integer.parseInt(fieldId.getText());       
        String nomb = fieldNom.getText();         
		c.setId(id_cat);
		c.setNombre(nomb);
        try{
        	ClienteBD cl = new ClienteBD();
        	if(request != ServidorBD.LIST){
        		cl.sendRequest(request,c);
	            list = (List) cl.sendRequest(ServidorBD.LIST_ALL);
	            mostrarLista(list);
        	}else{        		
        		c = (Categoria) cl.sendRequest(request,c);
        		areaResp.setText(c.toString());
        	}
            
            fieldId.setText("");
            fieldNom.setText("");
        }catch (IOException | ClassNotFoundException e) {
        	e.printStackTrace();
		}
	}

	public void presentarDatos(){
		try{
			ClienteBD cl = new ClienteBD();
			list = (List) cl.sendRequest(ServidorBD.LIST_ALL);
			mostrarLista(list);	 
		}catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "No se pudo establecer conexion","Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}

	public void mostrarLista(List list){
		areaResp.setText("");
		for(Object ca : list){
			areaResp.setText(areaResp.getText()+((Categoria)ca).toString());
			areaResp.setText(areaResp.getText()+"\n");
		}
	}	
}