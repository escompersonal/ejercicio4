import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.sql.SQLException;


public class ServidorBD {
    public static final int PORT = 5000;
    public static final int CREATE = 0;
    public static final int DELETE = 1;
    public static final int UPDATE = 2;
    public static final int LIST  = 3;
    public static final int LIST_ALL = 4;


    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException{
    	ServerSocket serverSocket =  new ServerSocket(PORT);
    	ObjectInputStream input = null;
        ObjectOutputStream output = null;
        Socket socketClient = null;
        int request;

    	ManagerConexion mc;
		Categoria c = null;

		while(true){
			System.out.println("Waiting ...");
			socketClient = serverSocket.accept();
			mc = new ManagerConexion();
			System.out.println("Connected");

			output = new ObjectOutputStream(socketClient.getOutputStream());
			
			input = new ObjectInputStream(socketClient.getInputStream());
			

			

			request = input.readInt();
			System.out.println(request);
			switch(request){
				case CREATE:
					c = (Categoria) input.readObject();
					output.writeObject(mc.alta(c));
				break;
				case DELETE:
					c = (Categoria) input.readObject();
					output.writeObject(mc.baja(c));
				break;
				case UPDATE:
					c = (Categoria) input.readObject();
					output.writeObject(mc.editar(c));
				break;
				case LIST:
				 	c = (Categoria) input.readObject();
					output.writeObject(mc.buscar(c));
				break;
				case LIST_ALL:
					output.writeObject(mc.listar());
				break;
			}
			input.close();
			output.close();
            socketClient.close();
		}
    	
    }
	
}